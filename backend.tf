terraform {
  backend "s3" {
    bucket = "demoterraform-remote-state"
    key    = "devops/terraform.tfstate"
    region = "ap-southeast-1"
  }
}
