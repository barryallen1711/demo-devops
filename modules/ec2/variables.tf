variable "private_subnet_id" {
  description = "ID of the private subnet"
  type        = string
}

variable "public_subnet_id" {
  description = "ID of the public subnet"
  type        = string
}

variable "private_vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "public_vpc_id" {
  description = "ID of the VPC"
  type        = string
}

variable "bucket_arn" {
  description = "arn of s3 bucket"
  type = string
}

variable "pub_key" {
  description = "public key"
}
