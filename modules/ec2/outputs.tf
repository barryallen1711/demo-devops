output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.demo_private_instance.id
}