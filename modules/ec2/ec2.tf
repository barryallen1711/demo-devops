resource "aws_key_pair" "key" {
  key_name   = "ssh_key"
  public_key = var.pub_key
}

resource "aws_iam_policy" "s3_access_policy" {
  name        = "S3AccessPolicy"
  description = "Allows GetObject and PutObject actions in a specific S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid       = "S3PutObject",
        Effect    = "Allow",
        Action    = [
          "s3:PutObject"
        ],
        Resource  = [
          "${var.bucket_arn}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_role" "ec2_s3_role" {
  name = "EC2S3InstanceRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "s3_policy_attachment" {
  name       = "ec2-s3-attachment"
  policy_arn = aws_iam_policy.s3_access_policy.arn
  roles      = [aws_iam_role.ec2_s3_role.name]
}

resource "aws_iam_instance_profile" "ec2_s3_profile" {
  name = "ec2_s3_profile"
  role = aws_iam_role.ec2_s3_role.name
}


resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic from public subnet"

  vpc_id      = var.private_vpc_id

  ingress {

    # we should allow incoming and outoging
    # TCP packets
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"

    # allow all traffic
    security_groups = [aws_security_group.public_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "public_sg" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic"

  vpc_id      = var.public_vpc_id

  ingress {

    # we should allow incoming and outoging
    # TCP packets
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"

    # allow all traffic
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "public sg"
  }
}



resource "aws_instance" "demo_private_instance" {
  ami           = "ami-0df7a207adb9748c7"
  instance_type = "t2.micro"
  subnet_id     = var.private_subnet_id 
  # refering key which we created earlier
  key_name        = aws_key_pair.key.key_name

  iam_instance_profile = aws_iam_instance_profile.ec2_s3_profile.name

  # refering security group created earlier
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  tags = {
    Name = "MyEC2Instance"
  }
}

resource "aws_instance" "demo_public_instance" {
  ami           = "ami-0df7a207adb9748c7"
  instance_type = "t2.micro"
  subnet_id     = var.public_subnet_id 
  # refering key which we created earlier
  key_name        = aws_key_pair.key.key_name


  # refering security group created earlier
  vpc_security_group_ids = [aws_security_group.public_sg.id]
  tags = {
    Name = "MyEC2PublicInstance"
  }
}
