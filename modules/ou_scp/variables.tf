variable "organization_root_id" {
  description = "The root ID of your AWS Organization"
  type = string
}