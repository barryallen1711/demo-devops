output "scp_id" {
  description = "ID of the SCP"
  value       = aws_organizations_policy.example_scp.id
}