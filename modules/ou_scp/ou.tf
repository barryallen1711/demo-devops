resource "aws_organizations_organizational_unit" "sandbox" {
  name     = "Sandbox"
  parent_id = var.organization_root_id
}

data "aws_iam_policy_document" "sanbox_policy" {
  statement {
    effect    = "Allow"
    actions   = ["ec2:*", "s3:GetObject", "s3:PutObject"]
    resources = ["*"]
  }
}

resource "aws_organizations_policy" "example_scp" {
  name        = "ExampleSCP"
  description = "Allow VPC, S3, and EC2 actions"

  # Your SCP policy document allowing VPC, S3, and EC2 actions
  content = data.aws_iam_policy_document.sanbox_policy.json
}

resource "aws_organizations_policy_attachment" "attach_scp_to_sandbox" {
  policy_id   = aws_organizations_policy.example_scp.id
  target_id   = aws_organizations_organizational_unit.sandbox.id
}
