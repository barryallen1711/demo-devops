variable "vpc1_cidr_block" {
  description = "CIDR block for the VPC"
}

variable "region" {
  type    = string
  default = "ap-southeast-1"
}

variable "vpc2_cidr_block" {
  description = "CIDR block for the VPC"
}

variable "vpc1_name" {
  description = "Name of the VPC"
}

variable "vpc2_name" {
  description = "Name of the VPC"
}

variable "private_subnet_cidr_block" {
  description = "CIDR block for private subnets"
  type        = string
}

variable "public_subnet_cidr_block" {
  description = "CIDR block for private subnets"
  type        = string
}

variable "private_availability_zones" {
  description = "availability zones for private subnets"
  type        = string
}

variable "public_availability_zones" {
  description = "availability zones for private subnets"
  type        = string
}