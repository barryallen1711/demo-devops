resource "aws_vpc" "private_vpc" {
  cidr_block = var.vpc1_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc1_name
  }
}

resource "aws_vpc" "public_vpc" {
  cidr_block = var.vpc2_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc2_name
  }
}

resource "aws_s3_bucket" "my_bucket" {
  bucket = "fptdemo-bucket" # Replace with your desired bucket name
  acl    = "private" # Set the ACL to control access
}

resource "aws_vpc_endpoint" "s3_endpoint" {
  vpc_id = aws_vpc.private_vpc.id
  service_name = "com.amazonaws.${var.region}.s3"
}

resource "aws_subnet" "private_subnet" {

  vpc_id                  = aws_vpc.private_vpc.id
  cidr_block              = var.private_subnet_cidr_block
  availability_zone       = var.private_availability_zones
  map_public_ip_on_launch = false

  tags = {
    Name = "Private-Subnet"
  }
}

resource "aws_subnet" "public_subnet" {

  vpc_id                  = aws_vpc.public_vpc.id
  cidr_block              = var.public_subnet_cidr_block
  availability_zone       = var.public_availability_zones
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-Subnet"
  }
}

// =========== Config public subnet can connect to outside world ============

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.public_vpc.id

  tags = {
    Name = "internet gateway"
  }
}

# Create a default route table for the public subnets
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.public_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    cidr_block = var.vpc1_cidr_block
    gateway_id = aws_vpc_peering_connection.vpc1_to_vpc2.id
  }

  tags = {
    Name = "Public-Route-Table"
  }
}

# Associate the private subnets with the public route table
resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

// =============== Config private subnet ===============

# Create a default route table for the private subnets
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.private_vpc.id

  route {
    cidr_block = var.vpc2_cidr_block
    gateway_id = aws_vpc_peering_connection.vpc1_to_vpc2.id
  }


  depends_on = [ aws_vpc_endpoint.s3_endpoint ]

  tags = {
    Name = "Private-Route-Table"
  }
}

resource "aws_vpc_endpoint_route_table_association" "example" {
  route_table_id  = aws_route_table.private_route_table.id
  vpc_endpoint_id = aws_vpc_endpoint.s3_endpoint.id
}


# Associate the private subnets with the private route table
resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}

# Create VPC peering connection from VPC1 to VPC2
resource "aws_vpc_peering_connection" "vpc1_to_vpc2" {
  peer_vpc_id = aws_vpc.public_vpc.id
  vpc_id = aws_vpc.private_vpc.id
  auto_accept = true  # This automatically accepts the peering request in this example
}
