output "vpc1_id" {
  description = "ID of the private VPC"
  value       = aws_vpc.private_vpc.id
}

output "vpc2_id" {
  description = "ID of the public VPC"
  value       = aws_vpc.public_vpc.id
}

output "private_subnet_id" {
  description = "ID of the private subnets"
  value       = aws_subnet.private_subnet.id
}

output "public_subnet_id" {
  description = "ID of the public subnets"
  value       = aws_subnet.public_subnet.id
}

output "s3_bucket_arn" {
  description = "Name of the S3 bucket"
  value       = aws_s3_bucket.my_bucket.arn
}
