module "ou_scp" {
  source = "./modules/ou_scp"
  
  organization_root_id = var.organization_root_id
}

module "vpc" {
  source = "./modules/vpc"
  
  vpc1_cidr_block = var.vpc1_cidr_block
  vpc2_cidr_block = var.vpc2_cidr_block
  vpc1_name = var.vpc1_name
  vpc2_name = var.vpc2_name
  private_subnet_cidr_block = var.private_subnet_cidr_block
  public_subnet_cidr_block = var.public_subnet_cidr_block
  private_availability_zones = var.private_availability_zones
  public_availability_zones = var.public_availability_zones
}

module "ec2" {
  source = "./modules/ec2"

  pub_key = var.pub_key
  bucket_arn = module.vpc.s3_bucket_arn
  private_vpc_id = module.vpc.vpc1_id
  public_vpc_id = module.vpc.vpc2_id
  public_subnet_id = module.vpc.public_subnet_id
  private_subnet_id = module.vpc.private_subnet_id
}